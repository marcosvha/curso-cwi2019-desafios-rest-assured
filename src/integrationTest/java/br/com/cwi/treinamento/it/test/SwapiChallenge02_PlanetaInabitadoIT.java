package br.com.cwi.treinamento.it.test;

import br.com.cwi.treinamento.it.baseTest.BaseTest;
import br.com.cwi.treinamento.it.category.FlightCheckTests;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.path.json.JsonPath.from;

@Slf4j
public class SwapiChallenge02_PlanetaInabitadoIT extends BaseTest {

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Swapi")
    @Description("Valide se existe um filme com maior número de planetas sem habitantes do que os demais")
    @DisplayName("Filme com maior número de planetas sem habitantes")
    public void existeFilmeComMaiorNumeroPlanetasInabitados() {

        int maiorNumeroPlanetasInabitados;
        String filmeMaiorNumeroPlanetasInabitados;


        // Caminho imaginado:

        // Obter lista de todos os filmes
        Response filmListResponse = given().baseUri("https://swapi.co/api/").
                when().
                get("films/");

        //ToDo: ver https://stackoverflow.com/questions/15531767/rest-assured-generic-list-deserialization
        ArrayList<String> films = filmListResponse.body().path("results");
        //Book[] books = listBookResponse.as(Book[].class);

        Iterator filmsIterator = films.iterator();
        while(filmsIterator.hasNext()) {
            String film = (String) filmsIterator.next();
            log.info("filme:" + film);

            // Para cada filme, Obter lista de planetas
            // Para cada planeta, ver numero de habitantes
            //      Se 0, incrementar contador de planetas inabitados do filme
            //      Se o numero planetas inabitados for maior que o previamente maior, substituir num 1 do ranking

        }


    }


}
