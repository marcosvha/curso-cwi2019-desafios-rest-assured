package br.com.cwi.treinamento.it.test;

import br.com.cwi.treinamento.hello.model.Availability;
import br.com.cwi.treinamento.hello.model.Book;
import br.com.cwi.treinamento.it.baseTest.BookFlowBaseTest;
import br.com.cwi.treinamento.it.category.FlightCheckTests;
import com.github.javafaker.Faker;
import com.google.gson.Gson;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
public class Challenge03_BookFlowIT extends BookFlowBaseTest {

    private static Book theBook;

    private static Book bookFactory() {
        if (null != theBook) {
            return theBook;
        }

        Faker faker = new Faker();
        String bookName = faker.book().title();
        theBook = new Book().builder()
                .name(bookName)
                .price(Double.valueOf(
                        faker.commerce().price(1, 399)
                                .replace(',', '.')))
                .build();
        return theBook;
    }

    @BeforeClass
    public static void setupBookFlow() {
        theBook = bookFactory();
        log.info("setupBookFlow: " + theBook.getName());
    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Book")
    @Description("Deve criar um livro")
    @DisplayName("Criação de Livro")
    public void T01_deveCriarUmLivro() {
        Response createBookResponse = given()
                .contentType(ContentType.JSON)
                .body(new Gson().toJson(theBook))
                .when()
                .post("/books/");
        createBookResponse
                .then().log().all()
                .statusCode(201);
        // ToDo: Tratar erro nome duplicado, outros status codes..
        // "Bug": não retorna ID do livro criado

        Response listBookResponse = given()
                .contentType(ContentType.JSON)
                .when()
                .get("/books/");
        listBookResponse
                .then().log().all()
                .statusCode(200)
                .body("$", hasSize(greaterThan(0)));

        // Obter ID do livro criado: como não tem um search, full scan, match pelo nome
        // https://stackoverflow.com/questions/15531767/rest-assured-generic-list-deserialization
        Book[] books = listBookResponse.as(Book[].class);

        for (Book oBook: books) {
            log.info(oBook.getName() + ": " + oBook.getBookId());
            if (theBook.getName().equals(oBook.getName())) {
                assertEquals(theBook.getPrice(), oBook.getPrice());
                theBook.setBookId(oBook.getBookId());
                break;
            }
        }

        assertNotNull(theBook.getBookId());
        log.info("ID do livro criado:" + theBook.getBookId());
    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Book")
    @Description("Deve atualizar estoque de um livro")
    @DisplayName("Atualização de estoque")
    public void T02_deveAtualizarEstoqueDeUmLivro() {
        assertNotNull(theBook.getBookId()); // Vai que, né..

        Response availabilityResponse = given()
                .contentType(ContentType.JSON)
                .when()
                .get(String.format("books/%d/availability", theBook.getBookId()));
        availabilityResponse
                .then().log().all()
                .statusCode(200)
                .body("bookId", equalTo(theBook.getBookId()));
//      JSON path bookId doesn't match.
//      Expected: <106L>
//          Actual: 106
//      Alterei para Integer, ao invés de Long no model

        Availability availability = availabilityResponse.as(Availability.class);

        log.info(String.format("Estoque atual do livro %s = %d", theBook.getName(), availability.getStock()));
        log.info("Adicionando 1 ao estoque...");

        Availability newAvailability = new Availability(availability.getBookId(), availability.getStock());
        newAvailability.setStock(availability.getStock() + 1);
        log.info("Nova disponibilidade deverá ser: " + newAvailability.getStock());

        Response updateAvailabilityResponse = given()
                .contentType(ContentType.JSON)
                .body(new Gson().toJson(newAvailability))
                .when()
                .put("/books/availability");
        updateAvailabilityResponse
                .then().log().all()
                .statusCode(204);

        // Verifica se realmente atualizou o estoque
        Response updatedAvailabilityResponse = given()
                .contentType(ContentType.JSON)
                .when()
                .get(String.format("books/%d/availability", theBook.getBookId()));
        updatedAvailabilityResponse
                .then().log().all()
                .statusCode(200)
                .body("stock", equalTo(newAvailability.getStock()));

        Availability updatedAvailability = updatedAvailabilityResponse.as(Availability.class);
        log.info("Disponibilidade atualizada: " + updatedAvailability.getStock());

        assertEquals(newAvailability.getStock(), updatedAvailability.getStock());
    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Book")
    @Description("Deve efetuar emprestimo de um livro")
    @DisplayName("Emprestimo de livro")
    public void T03_deveEfetuarEmprestimoDeUmLivro() {
        assertNotNull(theBook.getBookId()); // Vai que, né..

        Response loanResponse = given()
                .contentType(ContentType.JSON)
                .when()
                .put(String.format("books/%d/loan", theBook.getBookId()));
        loanResponse
                .then().log().all()
                .statusCode(204);
                //.body("bookId", equalTo(theBook.getBookId()));
/*
        {
            "timestamp": "2019-02-14T20:22:30.116+0000",
                "status": 500,
                "error": "Internal Server Error",
                "message": "401 Unauthorized",
                "path": "/books/115/loan"
        }
*/
    }


}
