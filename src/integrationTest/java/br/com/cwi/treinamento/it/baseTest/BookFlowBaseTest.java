package br.com.cwi.treinamento.it.baseTest;

import io.restassured.RestAssured;
import org.junit.BeforeClass;

public class BookFlowBaseTest extends BaseTest {
    @BeforeClass
    public static void setup() {
        RestAssured.baseURI = "http://book-store-demo.herokuapp.com";
        RestAssured.port = 80;
    }
}
