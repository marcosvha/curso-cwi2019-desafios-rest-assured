package br.com.cwi.treinamento.it.util;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class SwapiHelper {
    public static Response getFilmByName(String searchFilm) {
        return given().baseUri("https://swapi.co/api/").
                when().
                get("films/?search=" + searchFilm);
    }

    public static Response getPeopleByName(String searchName) {
        return given().baseUri("https://swapi.co/api/").
                when().
                get("people/?search=" + searchName);
    }

    public static int getFilmId(String searchFilm) {
        Response filmResponse = getFilmByName(searchFilm);
        assertEquals(filmResponse.getStatusCode(), 200);
        assertTrue(filmResponse.time() < 5000); // Esta request demora mais
        assertEquals(filmResponse.body().path("results[0].title"), searchFilm);
        return filmResponse.body().path("results[0].episode_id");
    }
}
