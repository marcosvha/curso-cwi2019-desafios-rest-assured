package br.com.cwi.treinamento.it.test;

import br.com.cwi.treinamento.it.baseTest.BaseTest;
import br.com.cwi.treinamento.it.category.FlightCheckTests;
import br.com.cwi.treinamento.it.util.SwapiHelper;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import static io.restassured.RestAssured.given;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SwapiChallenge01_YodaIT extends BaseTest {

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Swapi")
    @Description("Yoda não aparece no filme The Force Awakens")
    @DisplayName("Yoda não aparece no filme The Force Awakens")
    public void yodaAparecerNoFilmeTheForceAwakensNaoDeve() {

        String searchFilm = "The Force Awakens";
        String searchPeopleName = "Yoda";

        int filmId = SwapiHelper.getFilmId(searchFilm);
        Assert.assertNotNull(filmId);

        String urlFilm = String.format("https://swapi.co/api/films/%d/", filmId);

        Response peopleResponse = SwapiHelper.getPeopleByName(searchPeopleName);
        assertEquals(peopleResponse.getStatusCode(), 200);
        assertTrue(peopleResponse.time() < 1000);
        assertEquals(peopleResponse.body().path("results[0].name"), searchPeopleName);

        List<String> films = given().baseUri("https://swapi.co/api/").
                when().
                get("people/?search=" + searchPeopleName).
                path("results[0].films");
        Assert.assertNotNull(films);

        Assert.assertFalse( films.contains(urlFilm) );

        // Duvidas:
        // Como não repetir o given().baseUri ?

    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Swapi")
    public void deveEncontrarFilmeExistentePeloNome() {
        String searchFilm = "The Force Awakens";
        Response filmResponse = SwapiHelper.getFilmByName(searchFilm);

        assertEquals(filmResponse.getStatusCode(), 200);
        assertTrue(filmResponse.time() < 1000);
        assertEquals(filmResponse.body().path("results[0].title"), searchFilm);
    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Swapi")
    public void naoDeveEncontrarFilmeNaoExistentePeloNome() {
        String searchFilm = "Asdfdsa 4324$% *";
        Response filmResponse = SwapiHelper.getFilmByName(searchFilm);

        assertEquals(filmResponse.getStatusCode(), 200);
        assertTrue(filmResponse.time() < 1000);
        assertEquals((int)filmResponse.body().path("count"), 0);
        assertNotEquals(filmResponse.body().path("results[0].title"), searchFilm);
    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Swapi")
    public void deveEncontrarPessoaExistentePeloNome() {
        String searchName = "Yoda";
        Response peopleResponse = SwapiHelper.getPeopleByName(searchName);

        assertEquals(peopleResponse.getStatusCode(), 200);
        assertTrue(peopleResponse.time() < 1000);
        assertEquals(peopleResponse.body().path("results[0].name"), searchName);
    }

    @Test
    @Category(FlightCheckTests.class)
    @Severity(SeverityLevel.CRITICAL)
    @Feature("Swapi")
    public void naoDeveEncontrarPessoaNaoExistentePeloNome() {
        String searchName = "Asdfdsa 4324$% *";
        Response peopleResponse = SwapiHelper.getPeopleByName(searchName);

        assertEquals(peopleResponse.getStatusCode(), 200);
        assertTrue(peopleResponse.time() < 1000);
        assertEquals((int)peopleResponse.body().path("count"), 0);
        assertNotEquals(peopleResponse.body().path("results[0].name"), searchName);
    }

}
